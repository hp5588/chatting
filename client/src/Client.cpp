//
// Created by brian on 2016/4/21.
//

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <iostream>
#include "../include/Client.h"

Client::Client() {

}

Client::Client(string serverIp, uint16_t serverPort, string userName) {
    Client::userName = userName;
    Client::serverIP = serverIp;
    Client::serverPort = serverPort;
}

int Client::connectToServer() {
    /*open a socket*/
    socketFD = socket(AF_INET,SOCK_STREAM,0);
    if (socketFD<0) {
        logging.log(LOG_TYPE_FAIL,"socket open failed");
        return -1;
    }

    /*connect to server*/
    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(serverIP.c_str());
    address.sin_port = htons(serverPort);

    if (connect(socketFD,(sockaddr*)&address,sizeof(address))<0){
        logging.log(LOG_TYPE_FAIL,"connect failed");
        return -1;
    }

    logging.log(LOG_TYPE_DEBUG, "connect to server successfully");

    return 0;
}

int Client::disconnect() {
    Client::logout();
    close(socketFD);
    socketFD = -1;
    return 0;
}

int Client::sendTextToUsers(vector<string> usernames, string text) {
    vector<User> userList;
    for (string name : usernames){
        User user("",0,name);
        userList.push_back(user);
    }
    Message msg(MSG_TYPE_DATA);
    msg.setDstUsers(userList);
    msg.setDataString(text);
    Client::sendMessages(msg);

    return 0;
}

int Client::sendRegisterMessage() {
    Message registerMsg(MSG_TYPE_REGISTER);
    registerMsg.setDataString("MSG_TYPE_REGISTER");

    Client::sendMessages(registerMsg);

    return 0;
}


int Client::checkMessageBox() {
    string dataString;
    int returnMsgType = 0;
    if (Client::readString(dataString)<0){
//        logging.log(LOG_TYPE_DEBUG,"no message");
        return -1;
    }
    vector<Message> msgs = Message::messagesFromJsonString(dataString);

    for (Message msg : msgs) {
        switch (msg.getMessageType()) {
            case MSG_TYPE_CLIENT_GET_ADDRESS: {
                /*get the ip from what server see*/
                Client::ip = msg.getDstUser().front().getIP();
                Client::port = msg.getDstUser().front().getPort();
                returnMsgType = MSG_TYPE_CLIENT_GET_ADDRESS;
                break;
            }
            case MSG_TYPE_RSP_USER_OFFLINE: {
                logging.log(LOG_TYPE_INFO, msg.getDataString());
                break;
            }
            case MSG_TYPE_RSP_USER_NOT_EXIST: {
                logging.log(LOG_TYPE_INFO, msg.getDataString());
                break;
            }
            case MSG_TYPE_DATA: {
                /*print the receive message*/
                logging.log(LOG_TYPE_INFO,
                            "message from " + msg.getSrcUser().getUserName() + ": " + msg.getDataString());
                break;
            }
            case MSG_TYPE_SERVER_BROADCAST: {
                logging.log(LOG_TYPE_INFO,
                            "broadcast from " + msg.getSrcUser().getUserName() + ": " + msg.getDataString());
                break;
            }
            case MSG_TYPE_CLIENT_CHANGE_STATUS_REQUEST:{
                logging.log(LOG_TYPE_INFO, msg.getDataString());
                break;
            }
            case MSG_TYPE_LOGIN_SUCCESS:{
                loginStatus = MSG_TYPE_LOGIN_SUCCESS;
                logging.log(LOG_TYPE_INFO,msg.getDataString());
                break;
            }
            case MSG_TYPE_LOGIN_FAIL:{
                loginStatus = MSG_TYPE_LOGIN_FAIL;
                logging.log(LOG_TYPE_FAIL,msg.getDataString());
                break;
            }
        }
    }

    return returnMsgType;
}
int Client::sendPingMessage() {
    Message pingMsg(MSG_TYPE_PING);
    pingMsg.setDataString("ping from client " + Client::userName);

    Client::sendMessages(pingMsg);
    return 0;
}

int Client::sendMessages(Message message) {
    User me = Client::makeUser();

    /*mark the src user, timestamp*/
    message.setTimestampToNow();
    message.setScrUser(me);

    Client::writeString(message.toJsonString());
    return 0;
}



int Client::writeString(string dataString) {

    /*select socket see if available*/
    fd_set wfds;
    FD_ZERO(&wfds);
    FD_SET(socketFD,&wfds);
    timeval timeout;
    timeout.tv_sec =0 ;
    timeout.tv_usec = 10;

    uint32_t byteSent = 0;

    if (select(socketFD+1,NULL,&wfds,NULL,&timeout)>0) {
        /*send through socket*/
        byteSent = (uint32_t) write(socketFD, dataString.c_str(), dataString.size());
    } else{
        logging.log(LOG_TYPE_DEBUG,"socket to server is not available to write");
        return -1;
    }

    if(byteSent<0){
        logging.log(LOG_TYPE_FAIL,"data send failed with errno " +to_string(errno));
        return -1;
    }

    return byteSent;
}

int Client::readString(string &dataString) {

    /*select socket see if available*/
    fd_set rfds;
    FD_ZERO(&rfds);
    FD_SET(socketFD,&rfds);
    timeval timeout;
    timeout.tv_sec =0 ;
    timeout.tv_usec = 10;

    uint32_t byteReceive;

    if (select(FD_SETSIZE,&rfds,NULL,NULL,&timeout)>0) {
        char buffer[10000];
        memset(buffer,0, sizeof(buffer));
        /*read from socket*/
        byteReceive = (uint32_t) read(socketFD, buffer, sizeof(buffer));
        dataString = string(buffer);
    } else{
//        logging.log(LOG_TYPE_DEBUG,"socket to server is not available to read");
        return -1;
    }

    if(byteReceive<0){
        logging.log(LOG_TYPE_FAIL,"data read failed with errno " +to_string(errno));
        return -1;
    }
    return byteReceive;
}

int Client::registerUsername() {

    /*register and wait*/
    Client::sendRegisterMessage();
    Client::getUserName();

    registered = true;

    return 0;
}

int Client::waitIPMessage() {
    while (1){
        if(Client::checkMessageBox()==MSG_TYPE_CLIENT_GET_ADDRESS){
            return 0;
        }
        usleep(100000);
    }
}

bool Client::alreadyLogin() {
    if (loginStatus == MSG_TYPE_LOGIN_SUCCESS){
        return true;
    }
    return false;
}





int Client::login() {
    Message loginMsg = Message(MSG_TYPE_CLIENT_CHANGE_STATUS_REQUEST);
    loginMsg.setDataString("ONLINE");

    Client::sendMessages(loginMsg);

    /*wait response*/
    while (loginStatus!=-1){
        if (loginStatus==MSG_TYPE_LOGIN_FAIL){
            loginStatus = -1;
            /*return fail*/
            return -1;
        } else if (loginStatus==MSG_TYPE_LOGIN_SUCCESS){
            /*login successfully*/
            return 0;
        }
        usleep(100000);
    }

    return 0;
}
int Client::logout(){
    /*send goodbye message*/
    Message message(MSG_TYPE_CLIENT_CHANGE_STATUS_REQUEST);
    message.setDataString("OFFLINE");

    Client::loginStatus = -1;

    Client::sendMessages(message);
}






























