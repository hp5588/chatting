#include <iostream>
#include <Command.h>
#include <list>
#include <Logging.h>
#include <unistd.h>
#include <User.h>
#include <csignal>
#include "include/Client.h"
#include <pthread.h>
#include <Help.h>


using namespace std;

#define DEFAULT_IP "127.0.0.1"
#define DEFAULT_PORT 8080
int sk= -1;

void dieHandle(int signal){
    close(sk);
    Logging log("dieHandle", false);
    log.log(LOG_TYPE_INFO,"client is  with sig "+to_string(signal)+". socket "+ to_string(sk) + " is closing");
}


/*thread function*/
void *receiveThread(void * param);
void *pingThread(void *param);

/*modul*/
int connectToServer();

list<pair<string,string>> commandList = {
        {"connect <ip> <port>"              ,"connect to server"},
        {"login <username>"                 ,"login using username"},
        {"logout"                           ,"logout"},
        {"register <username>"              ,"register using username"},
        {"chat <username....> <content>"    ,"chat with other people"},
        {"bye"                              ,"close client"},
};

Client client;
Logging logging("main", true);
string ip = DEFAULT_IP;
uint16_t port = DEFAULT_PORT;
string userName;


int main() {
    Help helpPrinter(commandList);
    Command command;

    vector<string> cmds;
    string commandString;

    pthread_t pingThreadHandle, receiveThreadHandle;


    /*register kill signal*/
    signal(SIGKILL,dieHandle);


    while (1) {
        cout << "client>";
        getline(cin, commandString);
        command.extractCommand(commandString, cmds);


        if (cmds.at(0).compare("register") == 0) {
            /*parameter analysis */
            if (cmds.size()==2){
                userName = cmds.at(1);
            }  else{
                logging.log(LOG_TYPE_INFO, "parameter error");
                continue;
            }

            if(!client.isRegistered()){
                /*register with username if not yet registered*/
                client.setUserName(userName);
                client.registerUsername();
            }

        }else if (cmds.at(0).compare("connect") == 0){

            if (cmds.size()==1){
                /*default parameters*/
            }
            else if (cmds.size()==3) {
                ip = cmds.at(1);
                port = (uint16_t) stoi(cmds.at(2));
            }else{
                logging.log(LOG_TYPE_INFO, "parameter error");
                continue;
            }

            connectToServer();

        } else if (cmds.at(0).compare("login") == 0){
            if (cmds.size()==2) {
                userName = cmds.at(1);
            }else{
                logging.log(LOG_TYPE_INFO, "parameter error");
                continue;
            }
            /*check if connected*/
            if (client.getFD()<0){
                /*try to connect*/
                connectToServer();
            }


            /*check if login already*/
            if (client.alreadyLogin()){
                logging.log(LOG_TYPE_WARN, "please logout user "+client.getUserName()
                                       +" first before login using another user");
                continue;
            }

            /*try to login to server*/
            client.setUserName(userName);
            if (client.login()<0){
                continue;
            }

            /*create ping thread*/
            int result = pthread_create(&pingThreadHandle,NULL,pingThread,NULL);
            result += pthread_create(&receiveThreadHandle,NULL,receiveThread,NULL);

            if (result<0){
                logging.log(LOG_TYPE_FAIL,"ping thread create failed");
                return -1;
            }

        } else if (cmds.at(0).compare("logout") == 0) {
            pthread_cancel(pingThreadHandle);
            pthread_cancel(receiveThreadHandle);
            client.disconnect();


            continue;
        } else if (cmds.at(0).compare("chat") == 0) {
            int dstUserCount = cmds.size()-2;
            vector<string> users;
            if (cmds.size()<3){
                logging.log(LOG_TYPE_WARN,"parameter error");
                continue;
            }

            for (int i = 0; i < dstUserCount; ++i) {
                users.push_back(cmds.at(1+i));
            }
            client.sendTextToUsers(users,cmds.back());


        }else if (cmds.at(0).compare("bye") == 0) {
            client.disconnect();
            break;
        }else if (cmds.at(0).compare("help") == 0){
            helpPrinter.printHelp();
        } else{
            logging.log(LOG_TYPE_WARN,"command not found. Usage refer to help");
        }
    }
}

void *pingThread(void *param) {
    while (1) {
        /*send ping message*/
        client.sendPingMessage();
        sleep(2);
    }

}

void *receiveThread(void * param){
    while (1){
        client.checkMessageBox();
        usleep(10000);
    }
}

int connectToServer(){
    /*connect*/
    client = Client(ip,port,userName);
    if (client.connectToServer()<0){
        logging.log(LOG_TYPE_FAIL,"fail connect to server " + ip +":"+to_string(port));
        return -1;
    }

    logging.log(LOG_TYPE_INFO,"connected to "+ ip +":"+to_string(port));

    /*wait for ip retrieval*/
    client.waitIPMessage();

    logging.log(LOG_TYPE_INFO,"my external ip "+client.getClientIP()+":"+to_string(client.getClientPort()));
    return 0;
}


