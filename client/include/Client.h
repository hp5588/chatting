//
// Created by brian on 2016/4/21.
//

#ifndef CHATTING_CLIENT_H
#define CHATTING_CLIENT_H

#include <string>
#include <list>
#include <Logging.h>
#include <Message.h>

using namespace std;

class Client {
public:
    Client();
    Client(string serverIp, uint16_t serverPort, string userName);

    /*methods*/
    int connectToServer();
    int disconnect();
    int sendPingMessage();
    int waitIPMessage();
    int checkMessageBox();


    /*user functions*/
    int registerUsername();
    int login();
    int logout();
    bool alreadyLogin();
    int sendTextToUsers(vector<string> usernames, string text);



    const string &getServerIP() const {
        return serverIP;
    }
    const string &getClientIP() const {
        return ip;
    }

    void setServerIP(const string &serverIP) {
        Client::serverIP = serverIP;
    }
    void setClientIP(const string &clientIP) {
        Client::ip = clientIP;
    }

    uint16_t getServerPort() const {
        return serverPort;
    }
    uint16_t getClientPort() const {
        return port;
    }

    int getFD() {
        return socketFD;
    }

    void setServerPort(uint16_t serverPort) {
        Client::serverPort = serverPort;
    }

    const string &getUserName() const {
        return userName;
    }

    void setUserName(const string &userName) {
        Client::userName = userName;
    }

    bool isRegistered(){
        return registered;
    }

private:
    /*server parameters*/
    string serverIP;
    uint16_t serverPort;

    /*client parameters*/
    string ip;
    uint16_t port;
    string userName;
    bool registered = false;
    int loginStatus = -1;

    /*connection parameters*/
    int socketFD = -1;





    /*tools*/
    User makeUser(){
        User user(Client::ip,Client::port,Client::userName);
        user.setLastPingTimestampToNow();
        return user;
    }

    /*loggin*/
    Logging logging = Logging("Client", false);


    /*low level method*/
    int writeString(string dataString);
    int readString(string &dataString);

    /*message level send and receive*/
    int sendRegisterMessage();
    int sendMessages(Message message);

};


#endif //CHATTING_CLIENT_H
