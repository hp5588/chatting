cmake_minimum_required(VERSION 3.0)
project(server)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
find_package (Threads)

include_directories(${TOOLS_INCLUDE_DIRS})
include_directories(${COM_INCLUDE_DIRS})
include_directories(${jsoncpp_INCLUDE_DIRS})


set(SOURCE_FILES main.cpp src/Server.cpp src/Server.h)

add_executable(server ${SOURCE_FILES})
target_link_libraries(server tools communication pthread)
