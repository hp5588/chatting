#include <iostream>
#include <Command.h>
#include <list>
#include <Logging.h>
#include <unistd.h>
#include <Help.h>
#include "src/Server.h"

#define DEFAULT_IP "127.0.0.1"
#define DEFAULT_PORT 8080


using namespace std;

list<pair<string,string>> commandList = {
        {"run"      ,"start server"},
        {"help"     ,"show help"},
        {"quit"     ,"close server"}
};

Server server;


void *updateStatusBar(void *parameter);
int main() {

    Logging log("main", true);
    Help helpPrinter(commandList);
    Command command;

    vector<string> cmds;

    string commandString;


    while (1) {
        cout << "server>";
        getline(cin,commandString);
        command.extractCommand(commandString,cmds);


        if (cmds.at(0).compare("run")==0){
            string ip = DEFAULT_IP;
            uint16_t port = DEFAULT_PORT;

            if (cmds.size()==3){
                ip = cmds.at(1);
                port = (uint16_t) stoi(cmds.at(2));
            } else if (cmds.size() == 1){
                /*use default parameter*/
            } else{
                log.log(LOG_TYPE_INFO, "parameter error");
                continue;
            }


            pthread_t statusBarThreadHandle;
            server.setIp(ip);
            server.setPort(port);

            int result = pthread_create(&statusBarThreadHandle,NULL,updateStatusBar,NULL);

            server.run();
            exit(0);



            /*TODO fork later since it's better for debug*/
/*            int pid = fork();
            if (pid ==0){
                *//*child*//*

            } else if (pid >0){
                *//*parent*//*
                *//*another loop to wait for command*//*
            } else{
                *//*fork failed*//*
                log.log(LOG_TYPE_FAIL,"fork() failed");
            }*/



        } else if(cmds.at(0).compare("quit")==0){
            break;
        } else if(cmds.at(0).compare("help")==0){
            helpPrinter.printHelp();
        }
        else{
            cout << "Command not found" <<endl;
        }
    }

    return 0;
}


void *updateStatusBar(void *parameter){
    while (1){
        server.updateStatusBar();
        usleep(100000);
    }
}
