//
// Created by brian on 2016/4/16.
//

#include <User.h>
#include <list>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <Message.h>
#include <unistd.h>
#include <cstring>
#include "Server.h"

Server::Server() {

}

Server::Server(string ip, uint16_t port) {
    Server::ip = ip;
    Server::port = port;
}

void Server::run() {
    /*variables init*/
    bool on = 1;

    /*open socket*/
    socketFD = socket(AF_INET,SOCK_STREAM,0);

    if (socketFD < 0){
        logging.log(LOG_TYPE_FAIL,"socket open failed");
        return;
    }

    sockaddr_in address;
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = inet_addr(ip.c_str());
    address.sin_port = htons(port);

    /*bind*/
    if (bind(socketFD,(struct sockaddr *)&address,sizeof(address))<0){
        logging.log(LOG_TYPE_FAIL,"bind fail");
        return;
    }

    /*listen*/
    if (listen(socketFD,SOMAXCONN)<0){
        logging.log(LOG_TYPE_FAIL,"listen fail");
        return;
    }


    /*TODO give to child process!?*/
    /*while 1*/

    while (on) {

        /*accept*/
        /*select ready socket */
        /*read sockets and save to message queue*/
        /*write sockets from message queue*/
        Server::manageData();

        /*routing message*/
        /*may generate messages*/
        Server::routingMessage();

        /*check if users are still online by exam last ping*/
        Server::usersUpdate();

        /*clean up out-of-date messages in queue*/
        Server::messageCleanup();

        /*inform sender if receiver is not online*/
        Server::messageRecvOnlineCheckup();

        /*take a break*/
        usleep(10);
    }



}

void Server::manageData() {
    fd_set activatedFds;
    struct timeval tv;
    int retval;
    tv.tv_sec = 0;
    tv.tv_usec = IO_TIMEOUT;

    /*initialize*/
    FD_ZERO(&activatedFds);
    FD_SET(socketFD,&activatedFds);
    for (int fd : fds){
        FD_SET(fd,&activatedFds);
    }

    fd_set rfds = activatedFds
    , wfds = activatedFds
    , efds = activatedFds;


    retval = select(FD_SETSIZE,&rfds,&wfds,&efds,&tv);
    if (retval<0){
        logging.log(LOG_TYPE_FAIL,"select() failed with error " + to_string(errno));
        return;
    }


    sockaddr clientAddr;
    sockaddr_in *clientA = (sockaddr_in *) (&clientAddr);
    socklen_t addrLenght = sizeof(*clientA);

    /*someone is connecting*/
    if (FD_ISSET(socketFD, &rfds)) {
        /*accept*/
        int newFD = accept(socketFD, &clientAddr, &addrLenght);
        fds.push_back(newFD);
        char *addrChars = inet_ntoa(clientA->sin_addr);
        uint16_t clientPort = ntohs(clientA->sin_port);

        /*compose user according socket info*/
        User srcUser(string(addrChars), clientPort, "");
        srcUser.setFD(newFD);

        logging.log(LOG_TYPE_INFO,"new connection from " + srcUser.getIP() + ":" + to_string(srcUser.getPort()));

        /*add to waiting user list*/
        waitingUserList.push_back(srcUser);
        /*send client his ip address*/
        Message msg(MSG_TYPE_CLIENT_GET_ADDRESS);
        User serverUser = Server::makeServerUser();
        msg.setTimestampToNow();
        msg.setDstUser(srcUser);
        msg.setScrUser(serverUser);
        msg.setDataString("MSG_TYPE_CLIENT_GET_ADDRESS");

        outgoingQueue.push_back(msg);

        FD_CLR(socketFD, &rfds);
    }


    /*read data from users in waiting list*/
    /*upgrade to online user if identity correct*/
    for (User newUser : waitingUserList){
        //check if current available fd is for users in the waiting list
        if (FD_ISSET(newUser.getFD(),&rfds)){
            char buffer [5000];
            memset(buffer,0,sizeof(buffer));
            read(newUser.getFD(),buffer,sizeof(buffer));

            Message nMsg = Message(string(buffer));
            /*put correct fd*/
            User user = nMsg.getSrcUser();
            user.setFD(newUser.getFD());
            nMsg.setScrUser(user);

            //put into message queue
            incomingQueue.push_back(nMsg);
            FD_CLR(newUser.getFD(),&rfds);
        }
    }

    /*read data*/
    for (User user: onlineUserList){
        if (FD_ISSET(user.getFD(),&rfds)){
            char buffer [5000];
            memset(buffer,0,sizeof(buffer));
            int readBytes = (int) read(user.getFD(), buffer, sizeof(buffer));
            if (readBytes <= 0){
                /*check if disconnected*/
                int error = errno;
                if (error>0){
                    logging.log(LOG_TYPE_DEBUG, "socket error with error " + to_string(error));
                    continue;
                }
            }

//            logging.log(LOG_TYPE_DEBUG,"receive: \n"+ string(buffer));


            Message nMsg = Message(string(buffer));

            /*put into message queue*/
            incomingQueue.push_back(nMsg);
            FD_CLR(user.getFD(),&rfds);

        }
    }

    /*send data*/
    list<Message> messageToRemove;
    map<string,vector<Message>> messages;
    for(Message msg : outgoingQueue){
        /*send messages only to online or waiting list user*/
        /*user in online list*/
        User dstUser = msg.getDstUser().front();
        if(existInUserList(onlineUserList,dstUser)){
            /*update dstUser's info using online list to gain correct fd*/
            for (User u : onlineUserList){
                if (u==dstUser){
                    dstUser = u;
                    break;
                }
            }
            msg.setDstUser(dstUser);
        }else if(existInUserListIpAndPort(waitingUserList,dstUser)) {
            /*in waiting list*/
        } else {
            /*don't send this message yet*/
            continue;
        }

        messages[dstUser.getUserName()].push_back(msg);

    }
    for(pair<string,vector<Message>> userMsg : messages){
        /*if dst user socket is ready*/
        vector<Message> msgs = userMsg.second;

        int clientSocketFD = userMsg.second.front().getDstUser().front().getFD();
        if (FD_ISSET(clientSocketFD,&wfds)){
            /*write data to client*/
            string msgString = Message::messagesToJsonString(msgs);
            write(clientSocketFD,msgString.c_str(),msgString.length());

            /*pile up the message wait to be remove*/
            for (Message msg : msgs){
                messageToRemove.push_back(msg);
            }
            FD_CLR(clientSocketFD,&wfds);
        }
    }
    Server::removeMsgFromList(messageToRemove,outgoingQueue);


    /*try to read data from the rest fds*/
    for(int fd :fds){
        if (FD_ISSET(fd,&rfds)){
            /*try read some data*/
            char buffer [5000];
            memset(buffer,0,sizeof(buffer));
            int readBytes = (int) read(fd, buffer, sizeof(buffer));

            if (readBytes <= 0){
                /*check if disconnected*/
                int error = errno;
                if (error>0){
                    /*close and remove the fd*/
                    close(fd);
                    fds.remove(fd);
                    logging.log(LOG_TYPE_DEBUG, "socket error with error " + to_string(error));
                    continue;
                }
            }

            Message nMsg = Message(string(buffer));

            /*update user list with corresponding username*/
            User user;
            for (User u : userList){
                if (u==nMsg.getSrcUser()){
                    user = nMsg.getSrcUser();
                }
            }
            user.setFD(fd);
            userList.remove(user);
            userList.push_back(user);

            /*put into message queue*/
            nMsg.setScrUser(user);
            incomingQueue.push_back(nMsg);
            FD_CLR(fd,&rfds);
        }
    }

}

void Server::routingMessage() {

    list<Message> msgToRemove;
    /*deal with message according to type*/
    for (Message inMsg : incomingQueue){
        switch (inMsg.getMessageType()){
            case MSG_TYPE_CLIENT_CHANGE_STATUS_REQUEST:{

                if(inMsg.getDataString().compare("ONLINE")==0) {

                    /*if the user already exist in online list*/
                    if (existInUserList(onlineUserList,inMsg.getSrcUser())){
                        /*prompt user to logout first*/
                        Message loginFailMsg(MSG_TYPE_LOGIN_FAIL);
                        loginFailMsg.setDstUser(inMsg.getSrcUser());
                        loginFailMsg.setScrUser(Server::makeServerUser());
                        loginFailMsg.setDataString("user already login, please logout first.");

                        outgoingQueue.push_back(loginFailMsg);
                        break;
                    }

                    User targetUser;
                    /*compare port and ip in user list list*/
                    for (User user : userList) {
                        /*compare name*/
                        if (inMsg.getSrcUser().getUserName() == user.getUserName()) {
                            targetUser = inMsg.getSrcUser();
                        }
                    }

                    if (targetUser.getUserName().size() == 0) {
                        /*user invalid*/
                        logging.log(LOG_TYPE_FAIL, "invalid login message");
                        break;
                    }

                    /*update target user in user list*/
                    userList.remove(targetUser);
                    userList.push_back(targetUser);

                    /*login notification*/
                    for (User user: onlineUserList) {
                        Message notifyMsg(inMsg.getSrcUser(), {user},
                                          "user " + inMsg.getSrcUser().getUserName() + " is now online",
                                          MSG_TYPE_SERVER_BROADCAST, 5);

                        outgoingQueue.push_back(notifyMsg);
                    }
                    logging.log(LOG_TYPE_INFO, inMsg.getSrcUser().getUserName()+" is now online");


                    /*reply client's login request*/
                    Message rspMsg (MSG_TYPE_LOGIN_SUCCESS);
                    rspMsg.setDstUser(inMsg.getSrcUser());
                    rspMsg.setScrUser(Server::makeServerUser());
                    rspMsg.setDataString("successfully login");
                    outgoingQueue.push_back(rspMsg);


                    /*add to online list after login*/
                    onlineUserList.push_back(targetUser);

                    /*remove user from waiting list by compare ip or port*/
                    if (waitingUserList.size()>0) {
                        list<User>::iterator toDeleteIt;
                        for (list<User>::iterator it = waitingUserList.begin(); it != waitingUserList.end(); it++) {
                            if (it->getPort() == targetUser.getPort()) {
                                toDeleteIt = it;
                            }
                        }
                        waitingUserList.erase(toDeleteIt);
                    }

                } else if (inMsg.getDataString().compare("OFFLINE")==0) {
                    /*client need not to inform server when it offline*/
                    /*if requested, remove user from online list by adding time to the last ping making user timeout*/
                    for(User &user: onlineUserList){
                        if (user == inMsg.getSrcUser()){
                            user.setLastPingTimestampToNowWithOffset(-5);
                        }
                    }

                    /*client will be marked as offline by server when last ping is more than 3 sec from now*/
                }
                break;
            }
            case MSG_TYPE_DATA:{
                /*forward message*/
                /*tear origin message into different destination message*/
                vector<User> dstUsers = inMsg.getDstUser();

                list<User> userNotExist;
                for (User user : dstUsers){
                    /*TODO check if dst user exist*/
                    if (!existInUserList(userList,user)){
                        /*if not exist inform sender and remove ignore the receiver*/
                        userNotExist.push_back(user);
                        continue;
                    }


                        Message singleRecMessage = inMsg;
                    vector<User> dst= {user};
                    singleRecMessage.setDstUsers(dst);
                    outgoingQueue.push_back(singleRecMessage);

                    logging.log(LOG_TYPE_INFO,"forward message from "
                                              +inMsg.getSrcUser().getUserName()
                                              +" to "
                                              +user.getUserName());
                }

                /*generate the feeadback msg for client if destination does not exist*/
                if (userNotExist.size()>0) {
                    Message responseMsg(MSG_TYPE_RSP_USER_NOT_EXIST);
                    User srcUser = inMsg.getSrcUser();
                    User serverUser = Server::makeServerUser();

                    string userOffListString;
                    userOffListString += "user name <";
                    for (User user: userNotExist) {
                        userOffListString += user.getUserName() + " ";
                    }
                    userOffListString += "> not exist";

                    logging.log(LOG_TYPE_WARN,userOffListString);


                    responseMsg.setTimestampToNow();
                    responseMsg.setDstUser(srcUser);
                    responseMsg.setDataString(userOffListString);
                    responseMsg.setScrUser(serverUser);

                    outgoingQueue.push_back(responseMsg);
                }
                break;
            }
            case MSG_TYPE_PING:{
                /*update timeStamp for online user list and user list*/
                for (User &user : onlineUserList){
                    if (inMsg.getSrcUser()==user){
                        time_t now;
                        time(&now);
                        user.setLastPingTimestamp(now);

                        /*synchronously update user list*/
                        userList.remove(user);
                        userList.push_back(user);


                        logging.log(LOG_TYPE_DEBUG,"ping from user "+ user.getUserName());
                        break;
                    }
                }
                break;
            }
            case MSG_TYPE_SERVER_BROADCAST:{
                /*forward message*/
                vector<User> dstUsers = inMsg.getDstUser();
                for (User user : dstUsers){
                    Message singleRecMessage = inMsg;
                    vector<User> dst= {user};
                    singleRecMessage.setDstUsers(dst);
                    outgoingQueue.push_back(singleRecMessage);
                }
                break;
            }

            case MSG_TYPE_REGISTER:{
                User srcUser = inMsg.getSrcUser();
                /*look into waiting list*/
                /*TODO compare user name but user in waiting list doesn't have one yet*/
                if (!existInUserList(userList, srcUser)) {
                    /*remove from waiting list*/
                    /*manually compare ip and port since username was not available in waiting state*/
                    list<User>::iterator toDeleteIt;
                    for (list<User>::iterator it = waitingUserList.begin();it!=waitingUserList.end();it++) {
                        if ((srcUser.getIP().compare(it->getIP())==0)&&(srcUser.getPort()==it->getPort())){
                            /*remove this user from list*/
                            toDeleteIt = it;
                            break;
                        }
                    }

                    /*extract useful info from waiting list*//*ip&port&fd*/
                    User srcUser =  inMsg.getSrcUser();
                    srcUser.setFD(toDeleteIt->getFD());
                    srcUser.setLastPingTimestampToNow();

                    /*register new user*/
                    userList.push_back(srcUser);

                    /*remove the user from waiting list*/
                    waitingUserList.erase(toDeleteIt);


                    /*now we have user name and */
                    logging.log(LOG_TYPE_INFO, "user " + inMsg.getSrcUser().getUserName() + " registered successfully");

                } else {
                    /*TODO handle user already exist problem*/
                    logging.log(LOG_TYPE_WARN, "user duplication in userList. Named " + srcUser.getUserName());
                }

                break;

            }
            default:{
                /*msg type not recoginized*/
                break;
            }
        }
        msgToRemove.push_back(inMsg);
    }
    /*outgoing messages clean up*/
    for(Message msg : msgToRemove){
        incomingQueue.remove(msg);
    }



}

void Server::usersUpdate() {
    list<User> toInformUsersList = onlineUserList;
    list<User> nowOfflineUsersList;
    /*check if online's last ping is timeout*/
    list<User> toDelete;
    for (User user : onlineUserList){
        time_t  now;
        time(&now);
        if (difftime(now,user.getLastPingTimestamp())>ONLINE_PING_TIMEOUT){
            toDelete.push_back(user);
            /*add to offlinelist*/
            nowOfflineUsersList.push_back(user);
        }
    }
    if (nowOfflineUsersList.size()==0)
        return;


    /*cleanup*/
    Server::removeUserFromList(toDelete,onlineUserList);
    Server::removeUserFromList(toDelete,toInformUsersList);
    for(User user : toDelete){
        fds.remove(user.getFD());
        /*close connection*/
        close(user.getFD());
    }


    /*broadcast user offline message*/
    string offPeopleListString;
    vector<User> toInformUsersVector(toInformUsersList.begin(),toInformUsersList.end());
    for (User offUser : nowOfflineUsersList){
        Message offlineInformMsg(MSG_TYPE_SERVER_BROADCAST);
        offlineInformMsg.setTimestampToNow();
        offlineInformMsg.setDstUsers(toInformUsersVector);
        offlineInformMsg.setScrUser(offUser);
        offlineInformMsg.setDataString("offline");
        incomingQueue.push_back(offlineInformMsg);

        offPeopleListString.append(offUser.getUserName()).append(" ");

    }
    logging.log(LOG_TYPE_INFO, offPeopleListString + "is now offline");

}



void Server::messageRecvOnlineCheckup() {
    for(Message &msg : outgoingQueue){
        if (msg.getMessageType() == MSG_TYPE_RSP_USER_OFFLINE){
            /*ignore if the same type of message*/
            continue;
        }

        if (!existInUserList(onlineUserList , msg.getDstUser().front())&&!msg.getSendFail()){
            /*if the reciever is not online now*/
            /*inform sender right away*/
            Message responseMsg(MSG_TYPE_RSP_USER_OFFLINE);
            User srcUser = msg.getSrcUser();
            User dstUser = msg.getDstUser().front();

            /*mark message as send fail*/
            msg.setSendFail(true);

            for(User dstUpdatedUser : userList){
                if (dstUpdatedUser==dstUser){
                    /*user data with latest information(ping timestamps....)*/
                    dstUser = dstUpdatedUser;
                }
            }
            responseMsg.setDstUser(srcUser);
            responseMsg.setDataString("<" + dstUser.getUserName() + ">" +" is now offline, messages will be send when user is online");
            responseMsg.setScrUser(dstUser);/*set sender user as destination user*/
            responseMsg.setTimestampToNow();
            responseMsg.setTimeout(3);/*3 sec before being cleaned up*/

            outgoingQueue.push_back(responseMsg);
        }
    }

}


void Server::messageCleanup() {

    list<Message> toRemove;
    for (Message msg : outgoingQueue){
        if (msg.getMessageType()!=MSG_TYPE_DATA&&msg.getMessageType()!=MSG_TYPE_CLIENT_GET_ADDRESS){
            /*discard message if timeout except data message*/
            if (msg.isTimeout()){
                toRemove.push_back(msg);
            }
        }
    }

    for (Message msg : toRemove){
        outgoingQueue.remove(msg);
    }

}



bool Server::existInUserList(list<User> &userList, User &user) {
    for (User tuser : userList){
        if (tuser==user){
            /*duplication*/
            return true;
        }
    }
    return false;
}
bool Server::existInUserListIpAndPort(list<User> &userList, User &user) {
    for (User tuser : userList){
        if (tuser.getIP().compare(user.getIP())==0 && tuser.getPort()==user.getPort()){
            /*duplication*/
            return true;
        }
    }
    return false;
}

void Server::removeMsgFromList(list<Message> &targetMsgs, list<Message> &listToRemoveFrom) {
    for(Message msg : targetMsgs){
        listToRemoveFrom.remove(msg);
    }
}

void Server::removeUserFromList(list<User> &targetUsers, list<User> &listToRemoveFrom) {
    for(User user : targetUsers){
        listToRemoveFrom.remove(user);
    }
}

User & Server::makeServerUser() {
    User *serverUser = new User(Server::ip,Server::port,"server");
    return *serverUser;
}

void Server::updateStatusBar() {
    string online = to_string(Server::onlineUserList.size()) + " online ;";
    string registered = to_string(Server::userList.size()) + " registered ;";
    string queuedMessage = to_string(Server::outgoingQueue.size()) + " messages in queue ";
    cout << ">>STATUS: " + online + registered + queuedMessage + "  \r" <<flush;

}























