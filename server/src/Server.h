//
// Created by brian on 2016/4/16.
//

#ifndef CHATTING_SERVER_H
#define CHATTING_SERVER_H

/*1ms before time out*/
#define MSG_TIMEOUT 10 /*sec*/
#define IO_TIMEOUT 10 /*ms*/
#define ONLINE_PING_TIMEOUT 3 /*sec*/


#include <cstdlib>
#include <string>
#include <Message.h>

using namespace std;

class Server {
public:
    Server();
    Server(string ip, uint16_t port);
    void run();

    /*infomation*/
    void updateStatusBar();



    void setIp(string ip){
        Server::ip = ip;
    }
    void setPort (uint16_t port){
        Server::port = port;
    }


protected:
    string ip;
    uint16_t port;

private:

    /*data*/
    list<Message> incomingQueue;
    list<Message> outgoingQueue;
    list<User> waitingUserList;
    list<User> onlineUserList;
    list<User> userList;
    list<int> fds;

    /*variables*/
    int socketFD = -1;
    Logging logging = Logging("Server", false);

    /*status check*/
    bool isOnline(User &user);


    /*processes*/
    void manageData();
    void routingMessage();
    void usersUpdate(); //check if users are online
    void messageCleanup();
    void messageRecvOnlineCheckup();/*tell sender if message fail to send to destination*/

    /*tools*/
    static bool existInUserList(list<User> &userList, User &user);
    static bool existInUserListIpAndPort(list<User> &userList, User &user);
    static void removeMsgFromList(list<Message> &targetMsgs, list<Message> &listToRemoveFrom);
    static void removeUserFromList(list<User> &targetUsers, list<User> &listToRemoveFrom);
    User & makeServerUser();




};


#endif //CHATTING_SERVER_H
