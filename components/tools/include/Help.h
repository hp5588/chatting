//
// Created by brian on 2016/4/27.
//

#ifndef CHATTING_HELP_H
#define CHATTING_HELP_H

#include <cstdlib>
#include <string>
#include <list>

using namespace std;
class Help {
public:
    Help(list<pair<string,string>> commandList);
    void printHelp();
private:
    list<pair<string,string>> commandList;

};


#endif //CHATTING_HELP_H
