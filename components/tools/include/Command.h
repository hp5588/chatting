//
// Created by brian on 2016/4/14.
//

#ifndef CHATTING_COMMAND_H
#define CHATTING_COMMAND_H

#include <cstdlib>
#include <string>
#include <vector>

using namespace std;

class Command {
public:
    static int extractCommand(string fullString, vector<string> &commands);
};


#endif //CHATTING_COMMAND_H
