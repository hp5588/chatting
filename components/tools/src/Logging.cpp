//
// Created by brian on 2016/4/16.
//

#include <iostream>
#include <iomanip>
#include "../include/Logging.h"

Logging::Logging() {

}
Logging::Logging(string className, bool enable) {
    Logging::className = className;
    debugEnable = enable;
}
void Logging::log(string type, string info) {
    time_t now = time(0);
    tm *ltm = localtime(&now);


    if (type== LOG_TYPE_DEBUG){
        if (!debugEnable){
            return;
        }
        if (DEBUG == false){
            return;
        }
    }
    cout << "\r" << "["
    <<setw(2)<<right<< ltm->tm_hour<<":"
    <<setw(2)<<right<< ltm->tm_min<<":"
    <<setw(2)<<right<< ltm->tm_sec<<" "
    <<setw(7)<<left<< className <<"]"
    <<setw(5)<<left<< type << ": " << info
    << "                   "<< endl << flush;
}









