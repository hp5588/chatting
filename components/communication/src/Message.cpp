//
// Created by brian on 2016/4/14.
//

#include <iostream>
#include "../include/Message.h"

Message::Message(json jsonObj) {
    Message::fromJson(jsonObj);
}

Message::Message(uint8_t msgType) {
    Message::messageType = msgType;
    Message::setTimestampToNow();

    Message::timeout = DEFAULT_TIMEOUT;
    if (msgType == MSG_TYPE_SERVER_BROADCAST) {
        Message::srcUser = User();
        Message::messageType = MSG_TYPE_SERVER_BROADCAST;
        Message::dataString = "";
    } else if (msgType==MSG_TYPE_DATA){
        Message::timeout = 0;/*ultimate*/
    }
    else{
    }

}

Message::Message(const User srcUser, const vector<User> dstUsers, const string dataString, const uint8_t messageType,
                 const uint16_t timeout) {
    Message::srcUser = srcUser;
    Message::dstUsers = dstUsers;
    Message::dataString = dataString;
    Message::messageType = messageType;
    Message::timeout = timeout;
    Message::setTimestampToNow();
}
Message::Message() {
    Message::timeout = DEFAULT_TIMEOUT;
}


Message::Message(string dataString) {
    Message::timeout = DEFAULT_TIMEOUT;
    Message::fromJsonString(dataString);
}

string Message::toJsonString() {
    json root = Message::toJson();

    ostringstream stream;
    stream<<root.dump(4);
    /*DEBUG*/
    loggingMessage.log(LOG_TYPE_DEBUG, "to string:" +stream.str());


    return stream.str();
}

int Message::fromJsonString(string dataString) {
    if (dataString.size()==0){
        return -1;
    }

    json root = json::parse(dataString);
    Message::fromJson(root);

    return 0;
}

int Message::fromJson(json jsonObj) {
    /*parse JSON to user objects*/
    Message::srcUser = User(jsonObj["srcUser"]);
    json dstUsers = jsonObj["dstUsers"];
    if (!dstUsers.is_null()) {
        if (dstUsers.is_array()) {
            /*array*/
            for (json userJson : dstUsers) {
                Message::dstUsers.push_back(User(userJson));
            }
        } else{
            /*only one object*/
            Message::dstUsers.push_back(User(dstUsers));
        }
    }

    Message::dataString = jsonObj["dataString"];
    Message::timeStamp = jsonObj["timeStamp"];
    Message::timeout = (uint16_t) jsonObj["timeout"];
    Message::messageType = (uint8_t) jsonObj["messageType"];

    /*DEBUG*/
    loggingMessage.log(LOG_TYPE_DEBUG, "from string:" + dataString);
    return 0;
}


bool Message::isTimeout() {
    time_t now;
    time(&now);
    return (difftime(now, timeStamp) > timeout);

}

json Message::toJson() {
    json root;
    root["srcUser"] = Message::srcUser.toJson();
    root["dstUsers"];
    for (User user : Message::dstUsers){
        root["dstUsers"].push_back(user.toJson());
    }
    root["dataString"] = Message::dataString;
    long int time = (long int)Message::timeStamp;
    root["timeStamp"] = time;
    root["timeout"] = Message::timeout;
    root["messageType"] = Message::messageType;

    return root;
}

string Message::messagesToJsonString(vector<Message> messages) {
    json root;
    for (Message msg : messages){
        root.push_back(msg.toJson());
    }

    /*to sting*/
    ostringstream os;
    os << root.dump(4);

    return os.str();
}

vector<Message> Message::messagesFromJsonString(string jsonString) {
    json root;
    root = json::parse(jsonString);
    vector<Message> msgs;

    if (root.is_array()){
        Logging log("aaa",true);

        for (json msg : root){
            msgs.push_back(Message(msg));
        }
    }

    return msgs;
}
