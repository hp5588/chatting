//
// Created by brian on 2016/4/15.
//

#include <unistd.h>
#include <sys/socket.h>
#include <iostream>
#include "../include/User.h"
#include "../include/Message.h"

User::User() {

}


User::User(json jsonObj) {
    User::fromJson(jsonObj);
}

bool User::isConnected() {
 int error_code;
 socklen_t error_code_size = sizeof(error_code);
 getsockopt(fd, SOL_SOCKET, SO_ERROR, &error_code, &error_code_size);
 if (error_code==EPIPE){

    return false;
 }
    return false;
}

json User::toJson() {
    json root;
    root["userName"] = User::userName;
    root["port"] = User::port;
    root["ip"] = User::ip;
    /*TODO time_t conversion failed*/
    long int time = (long int)User::lastPingTimestamp;
    root["lastPingTimeStamp"] = time;

    ostringstream stream;
    stream << root;

    /*DEBUG*/
//    logging.log(LOG_TYPE_DEBUG, "to string:" +stream.str());
    return root;
}

void User::fromJsonString(string jsonString) {
    json root(jsonString);
    User::fromJson(root);

}

void User::fromJson(json jsonObj) {

    User user;
    User::setUserName(jsonObj["userName"]);
    User::setPort((uint16_t) jsonObj["port"]);
    User::setIp(jsonObj["ip"]);
    User::setLastPingTimestamp(jsonObj["lastPingTimeStamp"]);


}


















