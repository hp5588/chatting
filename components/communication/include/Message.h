//
// Created by brian on 2016/4/14.
//

#ifndef MESSAGE_H
#define MESSAGE_H

#define MSG_TYPE_DATA 1
#define MSG_TYPE_PING 2
#define MSG_TYPE_SERVER_BROADCAST 4
#define MSG_TYPE_CLIENT_GET_ADDRESS 5

#define MSG_TYPE_REGISTER 30
#define MSG_TYPE_REGISTER_SUCCESS 31
#define MSG_TYPE_REGISTER_FAIL 32


#define MSG_TYPE_CLIENT_CHANGE_STATUS_REQUEST 50
#define MSG_TYPE_LOGIN_SUCCESS 51
#define MSG_TYPE_LOGIN_FAIL 52

#define MSG_TYPE_RSP_USER_NOT_EXIST 99
#define MSG_TYPE_RSP_USER_OFFLINE 98

#define DEFAULT_TIMEOUT 5


#include <vector>
#include "User.h"
#include <json.hpp>

using json = nlohmann::json;



class Message {
protected:
    User srcUser;
    vector<User> dstUsers;
    string dataString;
    uint8_t messageType;
    time_t timeStamp;
    uint16_t timeout;/*second*/
    bool sendFail = false;

    Logging loggingMessage = Logging("Message", false);


public:
    /*constructor*/
    Message();
    Message(uint8_t msgType);
    Message(json jsonObj);
    Message(string dataString);
    Message(const User srcUser, const vector<User> dstUsers, const string dataString, const uint8_t messageType,
                const uint16_t timeout);

    /*check method*/
    bool isTimeout();


    /*export & import*/
    /*msgTyp:<msgTyp>;srcUser:<srcUser>;<dstUser1>,<dstUser2>...;<dataString> ;*/
    string toJsonString();
    int fromJsonString(string dataString);
    int fromJson(json jsonObj);



    /*setter*/
    void setDataString(string dataString){
        Message::dataString = dataString;
    }

    void setSendFail(bool sendFail){
        Message::sendFail = sendFail;
    }

    void setTimeout(uint16_t timeout){
        Message::timeout =  timeout;
    }


    void setScrUser(User &srcUser){
        Message::srcUser = srcUser;
    }
    void setDstUser(User &dstUser){
        vector<User> dstUsers = {dstUser};
        Message::dstUsers = dstUsers;
    }
    void setDstUsers(vector<User> &dstUsers){
        Message::dstUsers = dstUsers;
    }

    void setTimestamp(time_t time){
        Message::timeStamp = time;
    }
    void setTimestampToNow(){
        time_t now;
        time(&now);
        Message::timeStamp = now;
    }

    /*getter*/
    uint16_t getTimeout(){
        return timeout;
    }
    vector<User> getDstUser(){
        return dstUsers;
    }
    User&  getSrcUser(){
        return srcUser;
    }

    string getDataString(){
        return dataString;
    }
    json toJson();

    uint8_t  getMessageType(){
        return messageType;
    }
    bool getSendFail(){
        return sendFail;
    }


    /*definition for message to be the same*/
    bool operator == (Message msg){
        bool srcUser = (msg.getSrcUser()==this->getSrcUser());
        bool dstUser;
        if (msg.dstUsers.size()){
            dstUser = (msg.getDstUser().front()==this->getDstUser().front());
        }else{
            /*assume to be the same if no dstUser to compare*/
            dstUser = true ;
        }
        bool type = (msg.getMessageType()== this->messageType);
        bool data = (msg.getDataString()== this->dataString);
        return srcUser&&dstUser&&type&&data;
    }




    /*static tools*/
    static string messagesToJsonString(vector<Message> messages);
    static vector<Message> messagesFromJsonString(string jsonString);


};


#endif //CHATTING_MESSENGER_H
