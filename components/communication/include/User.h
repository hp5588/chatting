//
// Created by brian on 2016/4/15.
//

#ifndef CHATTING_USER_H
#define CHATTING_USER_H

#include <cstdlib>
#include <string>
#include <Logging.h>
#include <json.hpp>


using namespace std;
using json = nlohmann::json;


class User {
public:
    User();
    User(json jsonObj);
    User(const string &ip, uint16_t port, const string &userName) : ip(ip), port(port), userName(userName) {
        User::ip=ip;
        User::port = port;
        User::userName = userName;
    }


    /*json*/
    json toJson();

    void fromJsonString(string jsonString);
    void fromJson(json jsonObj);


    /*getter*/
    string getIP(){
        return User::ip;
    }
    int getFD(){
        return fd;
    }

    string getUserName()const {
        return userName;
    }
    time_t getLastPingTimestamp(){
        return lastPingTimestamp;
    }
    uint16_t getPort(){
        return User::port;
    }

    /*setter*/
    void setFD(int fd){
        User::fd = fd;
    }

    void setLastPingTimestamp(time_t lastPingTimestamp) {
        User::lastPingTimestamp = lastPingTimestamp;
    }

    void setLastPingTimestampToNow(){
        time_t now;
        time(&now);
        User::lastPingTimestamp = now;
    }

    void setLastPingTimestampToNowWithOffset(int offset){
        time_t now;
        time(&now);
        User::lastPingTimestamp = now + offset;
    }

    void setUserName(string userName){
        User::userName = userName;
    }
    void setIp(string ip){
        User::ip = ip;
    }
    void setPort(uint16_t port){
        User::port = port;
    }

    bool isConnected();


    /*operator*/
    bool operator == (const User &user) const{
        return (user.userName.compare(this->userName)==0);
    }



    bool operator< (const User &user) const{
        return user.userName.length() > this->userName.length();
    }

private:


protected:
    string ip;
    uint16_t port;
    string userName;
    int fd;
    time_t lastPingTimestamp;

    Logging logging = Logging("User", false);

};


#endif //CHATTING_USER_H
