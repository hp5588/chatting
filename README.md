# Chatting Server & Client

## Intro
Project contain both server side and client side code which can be built with cmake. Using json format for communication between client and server.

## Prerequisite
- cmake > 3.0
- g++ => 4.9 bugs reported with earlier version of compiler

## How to update compiler
> following commands are intended for ubuntu 14.04

1. echo deb http://ppa.launchpad.net/ubuntu-toolchain-r/test/ubuntu trusty main >> /etc/apt/sources.list
2. echo deb-src http://ppa.launchpad.net/ubuntu-toolchain-r/test/ubuntu trusty main >> /etc/apt/sources.list
3. apt-get update
4. apt-get install --force-yes -y gcc-4.9 g++-4.9
5. update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.9 50
6. update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.9 50
7. choose compilier using `update-alternatives g++` and  `update-alternatives gcc`

## Build
1. git clone https://gitlab.com/hp5588/chatting.git
2. cd chatting/
3. mkdir build
4. cd build/
5. cmake ..
6. make

## Run
1. in \<build> folder
2. there will be two folder, one for server another for client
3. cd server/ (or client/)
4. ./server (or ./client)


## Usage
### Server
> use help to see more avaliable commands

1. run (\<ip> \<port>)

> there is a status bar at bottom showing the number of online user and other info

### Client
> use help to see more avaliable commands

1. connect (\<ip> \<port>)
2. register \<username>
3. login \<username>
4. chat \<anotherUser> \<message>
5. bye

## Author
> 高伯文 B013012011