cmake_minimum_required(VERSION 3.0)
project(chatting)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

include_directories(json/src)
add_subdirectory(json)
add_subdirectory(components/tools)
add_subdirectory(components/communication)
add_subdirectory(server)
add_subdirectory(client)


